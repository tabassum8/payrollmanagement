import { Employee } from './Employee';

export class EmployeeSalary{
    employeeSalaryid:number;
    month:String;
    year:number;
    presentDays:number;
    netPay:number;
    paymentStatus:String;
    paymentMethod:String;
    emp:Employee;
}