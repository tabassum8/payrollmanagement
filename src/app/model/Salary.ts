import { Employee } from './employee';

export class Salary {
    sid: number;
    grade:String;
    amount: number;
    taxPercent: number;
    grossIncome: number;
    grossDeduction: number;

}