import { Salary } from './Salary';

export class Employee{
    eid:number;
    ename:String;
    username:String;
    password:String;
    deg:String;
    department:String;
    role:String;
    phNo:String;
    email:String;
    panNo:String;
    address:String;
    accountNo:String;
    location:String;
    dateOfJoining:String;
    salaryPerMonth:String;
    employeeStatus:String;
    bankName:String;
}