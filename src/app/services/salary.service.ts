import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Salary } from '../model/Salary';

@Injectable({
    providedIn:'root'
})
export class SalaryService{
    private baseUrl= 'http://localhost:5000/salary';

    salary: Salary;
    constructor(private http:HttpClient,private router: Router) { }
  

    addSalary(salary:Salary): Observable<Object>{
        
        return this.http.post('http://localhost:5000/salary' +`/saveSalary`,salary);
    }
  

    updateSalary(salary: Salary): Observable<Object> {
        return this.http.put('http://localhost:5000/salary' + `/updateSalary`, salary);
      }

    getAllSalaryDetail(){
        return this.http.get<Salary[]>('http://localhost:5000/salary'+`/getAllSalaries`);
    }

    deleteSalary(id):any
    {   
        return this.http.delete('http://localhost:5000/salary'+`/deleteSalary/${id}`);
    }


    getSalaryOfEmployee(salaryId){
          let params = new HttpParams();
          params = params.set('salaryId',salaryId);
          return this.http.get<Salary[]>('http://localhost:5000/salary'+`/getSalaryOfEmployee`,{params:params});
    }




}