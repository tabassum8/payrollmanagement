import { Injectable } from '@angular/core';
import { EmployeeSalary } from '../model/EmployeeSalary';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EmployeeSalaryService {

    empSalaryList: EmployeeSalary[];
    private baseUrl = 'http://localhost:5000/employeesalary';

    empSalary: EmployeeSalary;
    constructor(private http: HttpClient, private router: Router) { }


    addEmployeeSalary(empSalary: EmployeeSalary): Observable<Object> {

        return this.http.post('http://localhost:5000/employeesalary' + `/saveEmployeeSalary`, empSalary);
    }


    updateEmployeeSalary(employee: EmployeeSalary): Observable<Object> {
        return this.http.put('http://localhost:5000/employeesalary' + `/updateEmployee`, employee);
    }

    getAllEmployeeSalaryDetail() {
        return this.http.get<EmployeeSalary[]>('http://localhost:5000/employeesalary' + `/getAllEmployee`);
    }

    deleteEmployeeSalary(employeeId): any {
        debugger
        let params = new HttpParams();
        params = params.set('employeeId', employeeId);
        return this.http.delete('http://localhost:5000/employeesalary' + `/deleteEmp`, { params: params });
    }

    getEmployeeSalaryByMonth(month,year) {
      
        return this.http.get<EmployeeSalary[]>('http://localhost:5000/employeesalary' + `/getAllEmployeeSalaryByMonthAndYear/${month}/${year}`);
    }


    getEmployeeSalaryByMonthndId(id,month,year):any {
       
        return this.http.get<EmployeeSalary[]>('http://localhost:5000/employeesalary' + `/getEmployeeSalaryByEmpIdAndMonthAndYear/${id}/${month}/${year}`,);

    }

    getEmployeeSalaryById(emp) {
     
        return this.http.get<EmployeeSalary[]>('http://localhost:5000/employeesalary' + `/getEmployeeSalaryByEmpId/${emp}`);
    }

}