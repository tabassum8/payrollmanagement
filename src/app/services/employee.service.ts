import { Injectable } from '@angular/core';
import { Employee } from '../model/Employee';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Authenticate } from '../model/Authenticate';

@Injectable({
    providedIn:'root'
})
export class EmployeeService{
    employeeList:Employee[];
    private baseUrl= 'http://localhost:5000/employee';

    employee: Employee;
    constructor(private http:HttpClient,private router: Router) { }
  

   changePassword(emp:Authenticate):Observable<Object>
   {
       return this.http.put('http://localhost:5000/employee' + `/changePassword`, emp);
   }

    addEmployee(employee:Employee): Observable<Object>{
        
        return this.http.post('http://localhost:5000/employee' +`/saveEmployee`,employee);
    }
  

    updateEmployee(employee: Employee): Observable<Object> {
        return this.http.put('http://localhost:5000/employee' + `/updateEmployee`, employee);
      }

    getAllEmployeeDetail(){
        return this.http.get<Employee[]>('http://localhost:5000/employee'+`/getAllEmployee`);
    }

    getAllActiveEmployeeDetail(){
        return this.http.get<Employee[]>('http://localhost:5000/employee'+`/getAllActiveEmployee`);
    }

    deleteEmployee(id):any
    {
        return this.http.delete('http://localhost:5000/employee'+`/deleteEmp/${id}`);
    }


    getEmployeeById(id): Observable<any>{
        
          return this.http.get('http://localhost:5000/employee'+`/getEmployeeById/${id}`);
    }

    getEmployeeForPasswordVerify(pwd,id): any{
       
          return this.http.get('http://localhost:5000/employee'+`/getEmployeeForPwdVerify/${pwd}/${id}`);
    }



}