import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn:'root'
})
export class AuthenticationService{
    public empId;
    private baseUrl = 'http://localhost:5000/employee';
    private userDetails = new BehaviorSubject(null);
    constructor(private http: HttpClient) { }
  
    login(username, password): Observable<any> {
      let params = new HttpParams();
      params = params.set('username', username);
      params = params.set('password', password);
      return this.http.get(`${this.baseUrl}` + `/find`, { params: params });
    }
  
    sendUserDetail(data) {
      //console.log("admin details")
      this.userDetails.next(data);
      console.log(this.userDetails);
    }
  
    getAdminDetail() {
      return this.userDetails.asObservable();
    }
}