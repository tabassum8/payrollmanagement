import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean = false;
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,private router:Router,private authService:AuthenticationService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  onSubmit() {
   
    if (this.loginForm.invalid == true) {
      console.log("invalid")
      return;
    }
    this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)
      .subscribe(response => {
       // console.log(response.eid);

        if (response == null) {
          alert("please enter correct credentials");
          this.loginForm.reset();
        }
        
      
        if (response!=null) {
          this.authService.empId = response.eid;
          if (response.role == 'admin') {
            // this.studentService.sendData(response);

            this.authService.sendUserDetail(response);
            this.router.navigate(['/viewemployee']);

          } else
            if (response.role == 'employee') {

              this.authService.sendUserDetail(response);
              this.router.navigate(['/showpayslip']);
            }
        }
       
      });

  }
}
