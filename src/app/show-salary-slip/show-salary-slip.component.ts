import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeSalaryService } from '../services/empSalary.service';
import { AuthenticationService } from '../services/authentication.service';
import { EmployeeSalary } from '../model/EmployeeSalary';

@Component({
  selector: 'app-show-salary-slip',
  templateUrl: './show-salary-slip.component.html',
  styleUrls: ['./show-salary-slip.component.css']
})
export class ShowSalarySlipComponent implements OnInit {

  showSalaryForm:FormGroup;
  month:String;
  year:number;
  eid:number;
  empSalArr: EmployeeSalary[];
  empSal:EmployeeSalary= new EmployeeSalary();
  constructor(private formBuilder:FormBuilder,private router:Router,
    private empSalService:EmployeeSalaryService,private authService:AuthenticationService) { }

  ngOnInit(): void {

    this.showSalaryForm = this.formBuilder.group({
      month:['',[Validators.required]],
      year:['',[Validators.required]],
    });

    
    this.eid = this.authService.empId;
  
  }

  onSubmit(){
    console.log(this.showSalaryForm.controls);

    var form = this.showSalaryForm.controls;
    this.month = form.month.value;
    this.year = form.year.value;
    this.empSalService.getEmployeeSalaryByMonthndId(this.eid,this.month,this.year).subscribe(data => {
      this.empSalArr = data; console.log(data);
     // alert("slip generated successfully");
  })
    this.showSalaryForm.reset();

  }

}
