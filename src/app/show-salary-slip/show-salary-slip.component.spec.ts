import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSalarySlipComponent } from './show-salary-slip.component';

describe('ShowSalarySlipComponent', () => {
  let component: ShowSalarySlipComponent;
  let fixture: ComponentFixture<ShowSalarySlipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowSalarySlipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSalarySlipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
