import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/Employee';
import { EmployeeSalary } from '../model/EmployeeSalary';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import { EmployeeSalaryService } from '../services/empSalary.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp-salary-month-report',
  templateUrl: './emp-salary-month-report.component.html',
  styleUrls: ['./emp-salary-month-report.component.css']
})
export class EmpSalaryMonthReportComponent implements OnInit {

  salEmpArr: EmployeeSalary[];
  empSalFormByMonthYear:FormGroup;
  month: number;
  year:number;

  constructor(private formBuilder:FormBuilder,private employeeService:EmployeeService,
    private empSalService: EmployeeSalaryService, private router: Router) { }

  ngOnInit(): void {

    this.empSalFormByMonthYear= this.formBuilder.group({
      month:['',[Validators.required]],
      year:['',[Validators.required]],
    })
   
  }

  onSubmit(){
    var form= this.empSalFormByMonthYear.controls;
    this.month= form.month.value;
    this.year= form.year.value;
    this.empSalService.getEmployeeSalaryByMonth(this.month,this.year).subscribe(data => {
      this.salEmpArr = data;
  })
  if(this.salEmpArr==null){
    alert("no payment initiated");
  }
  this.empSalFormByMonthYear.reset();
  



  }

}
