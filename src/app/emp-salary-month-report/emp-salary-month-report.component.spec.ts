import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpSalaryMonthReportComponent } from './emp-salary-month-report.component';

describe('EmpSalaryMonthReportComponent', () => {
  let component: EmpSalaryMonthReportComponent;
  let fixture: ComponentFixture<EmpSalaryMonthReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpSalaryMonthReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpSalaryMonthReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
