import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
import { Employee } from '../model/Employee';
import { EmployeeSalary } from '../model/EmployeeSalary';
import { EmployeeSalaryService } from '../services/empSalary.service';

@Component({
  selector: 'app-pay-salary',
  templateUrl: './pay-salary.component.html',
  styleUrls: ['./pay-salary.component.css']
})
export class PaySalaryComponent implements OnInit {

  paySalaryForm: FormGroup;
  empArr: Employee[];
  i: number;
  empId: number;
  netPayAmt: any;
  pDay: any;
  finalAmount: number;
  emp: Employee = new Employee();
  empSal: EmployeeSalary = new EmployeeSalary();

  constructor(private formBuilder: FormBuilder, private employeeService: EmployeeService,
    private router: Router, private empSalService: EmployeeSalaryService) { }

  ngOnInit(): void {

    this.employeeService.getAllActiveEmployeeDetail().subscribe(data => {
      this.empArr = data;
    })

    this.paySalaryForm = this.formBuilder.group({

      eid: ['', [Validators.required]],

      month: ['', Validators.required],

      year:['',[Validators.required]],

      presentDays: ['', [Validators.required,
      Validators.maxLength(2),
      Validators.pattern('^[0-9]*$'),]],

      netPay: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],

      paymentStatus: ['', [Validators.required]],
      paymentMethod: ['', [Validators.required]]

    });
  }

  generateNetPay() {
    for (this.i = 0; this.i < this.empArr.length - 1; this.i++) {
      this.empId = this.paySalaryForm.controls['eid'].value;
      if (this.empArr[this.i].eid = this.empId) {
        this.emp = this.empArr[this.i];
        this.netPayAmt = this.empArr[this.i].salaryPerMonth;
        this.pDay = this.paySalaryForm.controls['presentDays'].value;
        this.netPayAmt = this.netPayAmt / 30;
        this.finalAmount = this.netPayAmt * this.pDay;
        this.paySalaryForm.controls['netPay'].setValue(this.finalAmount);

      }

    }
  }
  onSubmit() {

    console.log(this.paySalaryForm);
    if (this.paySalaryForm.invalid == true) {
      console.log("invalid");
      return;
    } else
      if (this.paySalaryForm.controls) {

        console.log(this.paySalaryForm.value);
        var form = this.paySalaryForm.controls;
        this.empSal.month = form.month.value;
        this.empSal.year= form.year.value;
        this.empSal.netPay = form.netPay.value;
        this.empSal.paymentMethod = form.paymentMethod.value;
        this.empSal.presentDays = form.presentDays.value;
        this.empSal.paymentStatus = form.paymentStatus.value;
        this.empSal.emp = this.emp;

        this.empSalService.addEmployeeSalary(this.empSal).subscribe(data => {
          console.log(data);
        });

        alert("payment successful");
        this.paySalaryForm.reset();
      }
  }


}
