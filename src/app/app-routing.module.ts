import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AddSalaryComponent } from './add-salary/add-salary.component';
import { ViewSalaryComponent } from './view-salary/view-salary.component';
import { UpdateSalaryComponent } from './update-salary/update-salary.component';
import { PaySalaryComponent } from './pay-salary/pay-salary.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { ShowSalarySlipComponent } from './show-salary-slip/show-salary-slip.component';
import { EmpSalaryReportComponent } from './emp-salary-report/emp-salary-report.component';
import { EmpSalaryMonthReportComponent } from './emp-salary-month-report/emp-salary-month-report.component';
import { EmpChangePasswordComponent } from './emp-change-password/emp-change-password.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HeaderPageComponent } from './header-page/header-page.component';
import { ActiveEmpListComponent } from './active-emp-list/active-emp-list.component';



const routes: Routes = [
  
  { path:'',component:HomePageComponent},
  { path:'login', component: LoginComponent },
  
  { path:'home', component: HomePageComponent },

  { path:'contactus', component: ContactusComponent },
  { path:'addemployee', component: AddEmployeeComponent },
  { path:'editemployee/:empObj', component: UpdateEmployeeComponent },
  { path:'viewemployee', component: ViewEmployeeComponent },
  { path:'admindashboard',component:AdminDashboardComponent},
  { path:'addsalary',component:AddSalaryComponent},
  { path:'viewsalary',component:ViewSalaryComponent},
  { path:'editsalary/:salObj',component:UpdateSalaryComponent},
  { path:'paysalary',component:PaySalaryComponent},
  { path:'empdashboard',component:EmployeeDashboardComponent},
  { path:'showpayslip',component:ShowSalarySlipComponent},
  { path:'empsalreport',component:EmpSalaryReportComponent},
  { path:'empsalmonthreport',component:EmpSalaryMonthReportComponent},
  { path:'empchangepwd',component:EmpChangePasswordComponent},
  { path:'adminchangepwd',component:AdminChangePasswordComponent},
  { path:'homepage',component:HomePageComponent},
  { path:'viewactiveemp',component:ActiveEmpListComponent},
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
