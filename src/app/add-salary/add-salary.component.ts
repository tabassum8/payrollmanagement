import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Salary } from '../model/Salary';
import { SalaryService } from '../services/salary.service';

@Component({
  selector: 'app-add-salary',
  templateUrl: './add-salary.component.html',
  styleUrls: ['./add-salary.component.css']
})
export class AddSalaryComponent implements OnInit {
  addSalaryForm: FormGroup;
  salSubmitted: boolean = false;
  sal: Salary = new Salary();
  amt: number;
  percent: number;
  grossAmount: number;
  grossDeduct: number;

  constructor(private formBuilder: FormBuilder,
    private router: Router, private salaryService: SalaryService) { }

  ngOnInit(): void {
    this.addSalaryForm = this.formBuilder.group({

      grade: ['', Validators.required],
      amount: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],
      grossIncome: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],
      taxPercent: ['', [Validators.required,
      Validators.maxLength(3),
      Validators.pattern('^[0-9]*$')]],
      grossDeduction: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],

    });

  }

  generateGrossIncome() {
    this.amt = this.addSalaryForm.controls['amount'].value;
    this.grossDeduct = this.addSalaryForm.controls['grossDeduction'].value;
    this.grossAmount = this.amt - this.grossDeduct;
    this.addSalaryForm.controls['grossIncome'].setValue(this.grossAmount);

  }
  generateGrossDeduction() {
    this.amt = this.addSalaryForm.controls['amount'].value;
    if (this.amt <= 250000) {
      this.percent = this.addSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 12500;
      this.addSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }
    else if (this.amt > 250000 || this.amt < 500000) {
      this.percent = this.addSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 12500;
      this.addSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }
    else if (this.amt >= 500000 || this.amt <= 1000000) {
      this.percent = this.addSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 12500;
      this.addSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }
    else {
      this.percent = this.addSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 112500;
      this.addSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }

  }

  onSubmit() {
    console.log(this.addSalaryForm);
    //alert("salary added successfully");
      if (this.addSalaryForm.invalid == true) {
        console.log("invalid");
        return;
      } else
        if (this.addSalaryForm.controls) {
          var form = this.addSalaryForm.controls;
          this.sal.grade = form.grade.value;
          this.sal.amount = form.amount.value;
          this.sal.taxPercent = form.taxPercent.value;
          this.sal.grossIncome = form.grossIncome.value;
          this.sal.grossDeduction = form.grossDeduction.value;



          if (this.sal) {
            this.salSubmitted = true;
            this.salaryService.addSalary(this.sal).subscribe(data => {
              console.log(data);
            });
            alert('Salary Added Successfully...!');
            this.addSalaryForm.reset();
            this.router.navigate(['/viewsalary']);
          }
    }

  }

}
