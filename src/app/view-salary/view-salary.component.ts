import { Component, OnInit } from '@angular/core';
import { Salary } from '../model/Salary';
import { SalaryService } from '../services/salary.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-salary',
  templateUrl: './view-salary.component.html',
  styleUrls: ['./view-salary.component.css']
})
export class ViewSalaryComponent implements OnInit {

  reload = false;
  salaryArr: Salary[];
  salary: Salary = new Salary();
  jsonData: any;
  constructor(private salaryService: SalaryService, private router: Router) { }

  ngOnInit(): void {
    this.salaryService.getAllSalaryDetail().subscribe(data => {
      this.salaryArr = data;
  })
  }

  deleteSalary(i) {
    console.log("salary id" + i)
    var con = confirm('Do you really want to delete?');
    if (con == true) {
      this.salaryService.deleteSalary(i).subscribe();
          this.salaryArr.splice(i);
          // alert("salary removed successfully");
          this.ngOnInit();
    }
    else {
      this.ngOnInit();
    }
  }

  updateSalary(e:Salary) {
    this.salary = e;
    this.jsonData = JSON.stringify(this.salary);
    console.log("Salary data after modifyclick:" + this.jsonData)
    this.router.navigate(['/editsalary', this.jsonData]);
}


}
