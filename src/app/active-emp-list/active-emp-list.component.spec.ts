import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveEmpListComponent } from './active-emp-list.component';

describe('ActiveEmpListComponent', () => {
  let component: ActiveEmpListComponent;
  let fixture: ComponentFixture<ActiveEmpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveEmpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveEmpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
