import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/Employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-active-emp-list',
  templateUrl: './active-emp-list.component.html',
  styleUrls: ['./active-emp-list.component.css']
})
export class ActiveEmpListComponent implements OnInit {
 
  reload = false;
  employeeArr: Employee[];
  employee: Employee = new Employee();
  jsonData: any;

  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    //this.employeeArr = [{ eid: 1234, ename: "Tabassum", username: "tabshaik", password: "", role: "junior", department: "Hr", deg: "associate", salary: this.sal }];
      this.employeeService.getAllActiveEmployeeDetail().subscribe(data => {
        this.employeeArr = data;
    })
  }


  deleteEmployee(e,i) {
    debugger
    console.log("EMployee id" + e.eid)
    var con = confirm('Do you really want to delete?');
    if (con == true) {
      e.employeeStatus="Inactive";
      this.employeeService.updateEmployee(e).subscribe(data => {
        console.log(data);
      });
      this.router.navigate(['/viewactiveemp']);
       
         
         // alert("Employee removed successfully");
          this.ngOnInit();
        
     
    }
    else {
      this.ngOnInit();
    }
  }

  updateEmployee(e:Employee) {
    this.employee = e;
    this.jsonData = JSON.stringify(this.employee);
    console.log("Employee data after modifyclick:" + this.jsonData)
    this.router.navigate(['/editemployee', this.jsonData]);
}


}
