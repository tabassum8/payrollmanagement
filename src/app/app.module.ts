import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';

import { ContactusComponent } from './contactus/contactus.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { HttpClientModule } from '@angular/common/http';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AddSalaryComponent } from './add-salary/add-salary.component';
import { ViewSalaryComponent } from './view-salary/view-salary.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { UpdateSalaryComponent } from './update-salary/update-salary.component';
import { PaySalaryComponent } from './pay-salary/pay-salary.component';
import { ShowSalarySlipComponent } from './show-salary-slip/show-salary-slip.component';
import { EmpSalaryReportComponent } from './emp-salary-report/emp-salary-report.component';
import { EmpSalaryMonthReportComponent } from './emp-salary-month-report/emp-salary-month-report.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { EmpChangePasswordComponent } from './emp-change-password/emp-change-password.component';

import { HomePageComponent } from './home-page/home-page.component';

import { HeaderPageComponent } from './header-page/header-page.component';
import { ActiveEmpListComponent } from './active-emp-list/active-emp-list.component';


@NgModule({
  declarations: [
    AppComponent,
 
    LoginComponent,
  
    ContactusComponent,
    AddEmployeeComponent,
    UpdateEmployeeComponent,
    ViewEmployeeComponent,
    AdminDashboardComponent,
    AddSalaryComponent,
    ViewSalaryComponent,
    EmployeeDashboardComponent,
    UpdateSalaryComponent,
    PaySalaryComponent,
    ShowSalarySlipComponent,
    EmpSalaryReportComponent,
    EmpSalaryMonthReportComponent,
    AdminChangePasswordComponent,
    EmpChangePasswordComponent,

    HomePageComponent,
   
    HeaderPageComponent,
    ActiveEmpListComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
