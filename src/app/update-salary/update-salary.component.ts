import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Salary } from '../model/Salary';
import { Router, ActivatedRoute } from '@angular/router';
import { SalaryService } from '../services/salary.service';

@Component({
  selector: 'app-update-salary',
  templateUrl: './update-salary.component.html',
  styleUrls: ['./update-salary.component.css']
})
export class UpdateSalaryComponent implements OnInit {
  obj: any;
  editSalaryForm: FormGroup;
  salaryList: Salary[] = [];
  modifySalarySubmitted: boolean = false;
  salary: Salary = new Salary();
  amt: any;
  grossDeduct: any;
  grossAmount: number;
  percent: any;

  constructor(private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute, private salaryService: SalaryService) { }

  ngOnInit(): void {
    this.editSalaryForm = this.formBuilder.group({

      grade: ['', Validators.required],
      amount: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],
      grossIncome: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],
      taxPercent: ['', [Validators.required,
      Validators.maxLength(3),
      Validators.pattern('^[0-9]*$')]],
      grossDeduction: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],

    });

    this.obj = Object(this.activatedRoute.snapshot.params['salObj'])
    console.log("In update salary" + this.obj);

    if (this.obj) {
      this.salaryList.push(JSON.parse(this.obj))
      console.log(JSON.parse(this.obj));

      this.editSalaryForm.controls['grade'].setValue(this.salaryList[0].grade);
      this.editSalaryForm.controls['amount'].setValue(this.salaryList[0].amount);
      this.editSalaryForm.controls['taxPercent'].setValue(this.salaryList[0].taxPercent);
      this.editSalaryForm.controls['grossIncome'].setValue(this.salaryList[0].grossIncome);
      this.editSalaryForm.controls['grossDeduction'].setValue(this.salaryList[0].grossDeduction);

    }

  }


  generateGrossIncome() {
    this.amt = this.editSalaryForm.controls['amount'].value;
    this.grossDeduct = this.editSalaryForm.controls['grossDeduction'].value;
    this.grossAmount = this.amt - this.grossDeduct;
    this.editSalaryForm.controls['grossIncome'].setValue(this.grossAmount);

  }
  generateGrossDeduction() {
    this.amt = this.editSalaryForm.controls['amount'].value;
    if (this.amt <= 250000) {
      this.percent = this.editSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 12500;
      this.editSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }
    else if (this.amt > 250000 || this.amt < 500000) {
      this.percent = this.editSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 12500;
      this.editSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }
    else if (this.amt >= 500000 || this.amt <= 1000000) {
      this.percent = this.editSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 12500;
      this.editSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }
    else {
      this.percent = this.editSalaryForm.controls['taxPercent'].value;
      this.grossDeduct = this.amt * this.percent / 100;
      this.grossDeduct = this.grossDeduct + 112500;
      this.editSalaryForm.controls['grossDeduction'].setValue(this.grossDeduct);
    }

  }

  onSubmit() {
    debugger
    console.log(this.editSalaryForm);
    if (this.editSalaryForm.invalid == true) {
      console.log("invalid")
      return;
    } else
      if (this.editSalaryForm.controls) {
        var form = this.editSalaryForm.controls;
        this.salary.sid=this.salaryList[0].sid;
        this.salary.grade = form.grade.value;
        this.salary.amount = form.amount.value;
        this.salary.taxPercent = form.taxPercent.value;
        this.salary.grossIncome = form.grossIncome.value;
        this.salary.grossDeduction = form.grossDeduction.value;
       


        if (this.salary) {
          this.modifySalarySubmitted = true;
          this.salaryService.updateSalary(this.salary).subscribe(data => {
            console.log(data);
          });
          alert('salary modified Successfully...!');
          this.editSalaryForm.reset();
          this.router.navigate(['/viewsalary']);
        }

      }
  }



}
