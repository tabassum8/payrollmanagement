import { Component, OnInit } from '@angular/core';
import { EmployeeSalaryService } from '../services/empSalary.service';
import { EmployeeSalary } from '../model/EmployeeSalary';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/Employee';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-emp-salary-report',
  templateUrl: './emp-salary-report.component.html',
  styleUrls: ['./emp-salary-report.component.css']
})
export class EmpSalaryReportComponent implements OnInit {

  empArr:Employee[];
  salEmpArr: EmployeeSalary[];
  eid: number;
  empSalFormById:FormGroup;
  constructor(private formBuilder:FormBuilder,private employeeService:EmployeeService,private empSalService: EmployeeSalaryService, private router: Router,private authService:AuthenticationService) { }

  ngOnInit(): void {

    this.empSalFormById= this.formBuilder.group({
      eid: ['', [Validators.required,
        Validators.maxLength(6),
        Validators.pattern('^[0-9]*$')]],
    })
   
  this.employeeService.getAllEmployeeDetail().subscribe(data => {
    this.empArr = data;
  })
  }

  onSubmit(){
    var form= this.empSalFormById.controls;
    this.eid= form.eid.value;
    this.empSalService.getEmployeeSalaryById(this.eid).subscribe(data => {
    this.salEmpArr = data;
    
  })
  
  this.empSalFormById.reset();
    



  }

}
