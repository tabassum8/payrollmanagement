import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Authenticate } from '../model/Authenticate';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Employee } from '../model/Employee';

@Component({
  selector: 'app-emp-change-password',
  templateUrl: './emp-change-password.component.html',
  styleUrls: ['./emp-change-password.component.css']
})
export class EmpChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  checkOldPwdForm: FormGroup;
  emp: Authenticate = new Authenticate();

  empArr: Employee[];
  oldPwd: String;
  displayNewPwd: boolean = false;
  displayOldPwd: boolean = false;

  constructor(private formBuilder: FormBuilder, private router: Router,
    private empService: EmployeeService, private authService: AuthenticationService) { }

  ngOnInit(): void {
    this.displayNewPwd = false;
    this.displayOldPwd = true;

    this.emp.empId = this.authService.empId;

    this.empService.getEmployeeById(this.emp.empId).subscribe(data => {

      if (data != null) {
        this.emp.username = data.username;
        console.log(this.emp.username);
      }

    })


    this.checkOldPwdForm = this.formBuilder.group({

      oldPassword: ['', [Validators.required]],
    })

    this.changePasswordForm = this.formBuilder.group({


      newPassword: ['', [Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],

      confirmPassword: ['', [Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
    })


  }


  onSubmitOldPwd() {
    var form = this.checkOldPwdForm.controls;
    this.oldPwd = form.oldPassword.value;
    this.empService.getEmployeeForPasswordVerify(this.oldPwd, this.emp.empId).subscribe(
      response => {
        if (response == true) {
          this.displayOldPwd = false;
          this.displayNewPwd = true;

        }
        else {
          this.displayNewPwd = false;
          alert("please enter correct password")
          this.checkOldPwdForm.reset();
        }
      }
    )


  }


  onSubmit() {


    console.log(this.changePasswordForm.controls);

    var form = this.changePasswordForm.controls;
    this.emp.newPassword = form.newPassword.value;
    this.emp.password = this.oldPwd;


    this.empService.changePassword(this.emp).subscribe(data => {

      console.log(data);
      this.changePasswordForm.reset();
      alert("Password changed successfully,Please login to continue");
      this.router.navigate(['/login']);

    });


  }


}
