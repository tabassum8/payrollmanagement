import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/Employee';
import { Salary } from '../model/Salary';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {

  reload = false;
  employeeArr: Employee[];
  sal: Salary;
  employee: Employee = new Employee();
  jsonData: any;

  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    //this.employeeArr = [{ eid: 1234, ename: "Tabassum", username: "tabshaik", password: "", role: "junior", department: "Hr", deg: "associate", salary: this.sal }];
      this.employeeService.getAllEmployeeDetail().subscribe(data => {
        this.employeeArr = data;
    })
  }


  deleteEmployee(e,i) {
    debugger
    console.log("EMployee id" + e.eid)
    var con = confirm('Do you really want to delete?');
    if (con == true) {
      this.employeeService.deleteEmployee(e.eid).subscribe();
       
          this.employeeArr.splice(i);
          alert("Employee removed successfully");
          this.ngOnInit();
        
     
    }
    else {
      this.ngOnInit();
    }
  }

  updateEmployee(e:Employee) {
    this.employee = e;
    this.jsonData = JSON.stringify(this.employee);
    console.log("Employee data after modifyclick:" + this.jsonData)
    this.router.navigate(['/editemployee', this.jsonData]);
}

}
