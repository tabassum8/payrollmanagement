import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/Employee';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  obj: any;
  editEmployeeForm: FormGroup;
  employeeList: Employee[] = [];
  modifyEmployeeSubmitted: boolean = false;
  employee: Employee = new Employee();

  constructor(private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute, private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.editEmployeeForm = this.formBuilder.group({

      eid: ['', [Validators.required,
      Validators.maxLength(6),
      Validators.pattern('^[0-9]*$')]],

      ename: ['', [Validators.required,
      Validators.pattern('^[a-zA-Z ]*$')]],

      username: ['', [Validators.required,
      Validators.pattern('^[a-zA-Z ]*$')]],

      password: ['', [Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],

      deg: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],

      department: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],

      role: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],


      email: ['', [Validators.required,
      Validators.email,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],

      phNo: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],

      panNo: ['', [Validators.required,
      Validators.maxLength(10)]],

      accountNo: ['', [Validators.required,
      Validators.maxLength(12),
      Validators.pattern('^[0-9]*$')]],

      location: ['', [Validators.required,
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z ]*$')]],

      address: ['', [Validators.required,
      Validators.maxLength(50)]],

      dateOfJoining: ['', Validators.required],

      bankName: ['', Validators.required],

      employeeStatus: ['', Validators.required],

      salaryPerMonth: ['', [Validators.required, Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],

    });


    this.obj = Object(this.activatedRoute.snapshot.params['empObj'])
    console.log("In update employee" + this.obj);

    if (this.obj) {
      this.employeeList.push(JSON.parse(this.obj))
      console.log(JSON.parse(this.obj));

      this.editEmployeeForm.controls['eid'].setValue(this.employeeList[0].eid);
      this.editEmployeeForm.controls['ename'].setValue(this.employeeList[0].ename);
      this.editEmployeeForm.controls['username'].setValue(this.employeeList[0].username);
      this.editEmployeeForm.controls['password'].setValue(this.employeeList[0].password);
      this.editEmployeeForm.controls['deg'].setValue(this.employeeList[0].deg);
      this.editEmployeeForm.controls['department'].setValue(this.employeeList[0].department);
      this.editEmployeeForm.controls['role'].setValue(this.employeeList[0].role);
      this.editEmployeeForm.controls['salaryPerMonth'].setValue(this.employeeList[0].salaryPerMonth);
      this.editEmployeeForm.controls['employeeStatus'].setValue(this.employeeList[0].employeeStatus);
      this.editEmployeeForm.controls['bankName'].setValue(this.employeeList[0].bankName);
      this.editEmployeeForm.controls['dateOfJoining'].setValue(this.employeeList[0].dateOfJoining);
      this.editEmployeeForm.controls['address'].setValue(this.employeeList[0].address);
      this.editEmployeeForm.controls['location'].setValue(this.employeeList[0].location);
      this.editEmployeeForm.controls['accountNo'].setValue(this.employeeList[0].accountNo);
      this.editEmployeeForm.controls['panNo'].setValue(this.employeeList[0].panNo);
      this.editEmployeeForm.controls['phNo'].setValue(this.employeeList[0].phNo);
      this.editEmployeeForm.controls['email'].setValue(this.employeeList[0].email);

    }
  }

  onSubmit() {
    debugger
    console.log(this.editEmployeeForm);
   
      if (this.editEmployeeForm.controls) {
        var form = this.editEmployeeForm.controls;
        this.employee.eid = this.employeeList[0].eid;
        this.employee.ename = form.ename.value;
        this.employee.username = form.username.value;
        this.employee.password = form.password.value;
        this.employee.deg = form.deg.value;
        this.employee.department = form.department.value;
        this.employee.role = form.role.value;

        this.employee.panNo = form.panNo.value;
        this.employee.phNo = form.phNo.value;
        this.employee.email = form.email.value;
        this.employee.bankName = form.bankName.value;
        this.employee.accountNo = form.accountNo.value;
        this.employee.location = form.location.value;
        this.employee.address = form.address.value;
        this.employee.employeeStatus = form.employeeStatus.value;
        this.employee.dateOfJoining = form.dateOfJoining.value;
        this.employee.salaryPerMonth = form.salaryPerMonth.value;

        if (this.employee) {
          this.modifyEmployeeSubmitted = true;
          this.employeeService.updateEmployee(this.employee).subscribe(data => {
            console.log(data);
          });
          alert('Employee modified Successfully...!');
          this.editEmployeeForm.reset();
          this.router.navigate(['/viewemployee']);
        }

      }
  }

}
