import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/Employee';
import { Salary } from '../model/Salary';
import { SalaryService } from '../services/salary.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  empSubmitted: boolean = false;
  addEmployeeForm: FormGroup;
  emp: Employee = new Employee();
  salaryArr: Salary[];
  selectedDeg: any;
  i:number;
  sal:number;

  constructor(private formBuilder: FormBuilder,
    private router: Router, private employeeService: EmployeeService,private salaryService:SalaryService) { }

  ngOnInit(): void {
    this.salaryService.getAllSalaryDetail().subscribe(data => {
      this.salaryArr = data;
    });
    this.addEmployeeForm = this.formBuilder.group({

      eid: ['', [Validators.required,
      Validators.maxLength(6),
      Validators.pattern('^[0-9]*$')]],

      ename: ['', [Validators.required,
      Validators.pattern('^[a-zA-Z ]*$')]],

      username: ['', [Validators.required,
      Validators.pattern('^[a-zA-Z ]*$')]],

      password: ['', [Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],

      deg: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],

      department: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],

      role: ['', [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z ]*$')]],


      email: ['', [Validators.required,
      Validators.email,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],

      phNo: ['', [Validators.required,
      Validators.maxLength(10),
      Validators.pattern('^[0-9]*$')]],

      panNo: ['', [Validators.required,
      Validators.maxLength(10)]],

      accountNo: ['', [Validators.required,
      Validators.maxLength(12),
      Validators.pattern('^[0-9]*$')]],

      location: ['', [Validators.required,
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z ]*$')]],

      address: ['', [Validators.required,
      Validators.maxLength(50)]],

      dateOfJoining: ['', Validators.required],

      bankName: ['', Validators.required],

      employeeStatus: ['', Validators.required],

      salaryPerMonth: ['', [Validators.required, Validators.maxLength(30)]],

    });

  }


  generateSalary(){
    this.selectedDeg= this.addEmployeeForm.controls['deg'].value;
  
    for(this.i=0;this.i<this.salaryArr.length;this.i++){
      if(this.selectedDeg==this.salaryArr[this.i].grade){
        this.sal=this.salaryArr[this.i].grossIncome;
        this.sal = this.sal/12;
        this.addEmployeeForm.controls['salaryPerMonth'].setValue(this.sal);
      }
    }

  }
  onSubmit() {
    console.log(this.addEmployeeForm.controls);
    if (this.addEmployeeForm.invalid == true) {
      console.log("invalid");
      return;
    } else
      if (this.addEmployeeForm.controls) {
        var form = this.addEmployeeForm.controls;
        this.emp.eid = form.eid.value;
        this.emp.ename = form.ename.value;
        this.emp.deg = form.deg.value;
        this.emp.username = form.username.value;
        this.emp.password = form.password.value;
        this.emp.department = form.department.value;
        this.emp.role = form.role.value;
        this.emp.accountNo = form.accountNo.value;
        this.emp.bankName= form.bankName.value;
        this.emp.address = form.address.value;
        this.emp.location = form.location.value;
        this.emp.panNo = form.panNo.value;
        this.emp.dateOfJoining = form.dateOfJoining.value;
        this.emp.salaryPerMonth = form.salaryPerMonth.value;
        this.emp.email = form.email.value;
        this.emp.phNo = form.phNo.value;
        this.emp.employeeStatus = form.employeeStatus.value;



        if (this.emp) {
          this.empSubmitted = true;
          this.employeeService.addEmployee(this.emp).subscribe(data => {
            console.log(data);
          });
          alert('Employee Added Successfully...!');
          this.addEmployeeForm.reset();
           this.router.navigate(['/viewemployee']);
        }

      }
  }


}
